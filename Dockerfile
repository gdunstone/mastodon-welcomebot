FROM alpine


RUN apk add --no-cache py3-pip py3-requests

RUN pip3 install --no-cache-dir Mastodon.py

ENV MASTODON_ACCESS_TOKEN ""
ENV MASTODON_API_BASE_URL ""
ENV PYTHONUNBUFFERED=1

WORKDIR /usr/src/app
COPY . .

CMD ["python3", "run.py"]