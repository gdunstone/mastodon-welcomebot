#!/usr/bin/env python3
from pprint import pprint
import os
import time
import random
from mastodon import Mastodon, StreamListener
from string import Template
from glob import glob
"""
this bot substitutes the following accounts user dict into template.tmpl 
and then dms the user with that message
"""
mastodon = Mastodon(
    access_token=os.environ.get("MASTODON_ACCESS_TOKEN"),
    api_base_url=os.environ.get("MASTODON_API_BASE_URL")
)


templ_base = "Welcome, @${acct}! "
templ_number = "This is welcome message {}/{}\n\n"

templates = []
tfiles = sorted(list(glob("templates/*.tmpl")))
print(f"Loading {len(tfiles)} templates from disk")
print(f"{str(tfiles)}")
for i, tfile in enumerate(tfiles):
    with open(tfile) as f:
        t_head = templ_base + templ_number.format(i + 1, len(tfiles))
        templates.append(Template(t_head + f.read()))

externalmessage = """
Hi @${acct}, thanks for your interest in this bot.

Unfortunately it only welcomes people to one instance.

It is published on dockerhub under stormv/mastodon-welcomebot

The source code to build your own docker image is at: gitlab.org/gdunstone/mastodon-welcomebot
"""


class WelcomeListener(StreamListener):
    def on_notification(self, notification):
        if notification.type == "follow":
            pprint(notification)
            sleeptime = random.uniform(10.0, 20.0)
            print(f"Sleeping for {sleeptime:.2f}s")
            time.sleep(sleeptime)
            # check to see if the account is not from round here
            if "@" in notification.account.acct:
                mastodon.status_post(Template(externalmessage).substitute(**notification.account), visibility="direct")
                return
            # iterate over templates and smash in the account details
            for template in templates:
                msg = template.substitute(
                    **notification.account
                )
                mastodon.status_post(msg, visibility="direct")
                sleeptime = random.uniform(1.0, 5)
                print(f"Sleeping for {sleeptime:.2f}s")
                time.sleep(sleeptime)

            mastodon.notifications_dismiss(notification['id'])


mastodon.stream_user(WelcomeListener())
